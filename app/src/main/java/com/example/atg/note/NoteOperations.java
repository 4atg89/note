package com.example.atg.note;

public interface NoteOperations {

    void deleteANote(String fileName);

    void openTheNote(String fileName);

}
