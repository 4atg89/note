package com.example.atg.note;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class TextSizeAdapter extends BaseAdapter {

    private List<Integer> mTextSize;
    private Context mContext;

    public TextSizeAdapter(Context context,List<Integer> textColors) {
        mContext = context;
        mTextSize = textColors;

    }


    @Override
    public int getCount() {
        return mTextSize.size();
    }

    @Override
    public Integer getItem(int position) {
        return mTextSize.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.setting_text_color,null);
        TextView tvSize = (TextView) view.findViewById(R.id.tvColorSetting);
        if(position == 0){
            tvSize.setText("Выберите размер текста.");
        }else{
            tvSize.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    mContext.getResources().getDimension(mTextSize.get(position)));
            tvSize.setText("Выбрать");
        }


        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.setting_text_color,null);
            holder = new ViewHolder();
            holder.tvSize = (TextView)convertView.findViewById(R.id.tvColorSetting);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        if(position == 0){
            holder.tvSize.setText("Выберите размер текста.");
        }else{
            holder.tvSize.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    mContext.getResources().getDimension(mTextSize.get(position)));
            holder.tvSize.setText("Выбрать");
        }

        return convertView;
    }

    private class ViewHolder{

        TextView tvSize;
    }
}
