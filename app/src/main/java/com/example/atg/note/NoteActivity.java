package com.example.atg.note;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

public class NoteActivity extends AppCompatActivity implements View.OnClickListener {

    private FloatingActionButton mFabEditAndSave;
    private EditText etNote, etTitle;
    private String fileName;

    private SharedPreferences prefs;
    private SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        fileName = intent.getStringExtra(MainActivity.NOTE_PATH);

        etNote = (EditText) findViewById(R.id.etNote);
        etTitle = (EditText) findViewById(R.id.etTitle);
        mFabEditAndSave = (FloatingActionButton) findViewById(R.id.fabEditAndSave);
        mFabEditAndSave.setOnClickListener(this);

        if(fileName != null){
            etNote.setText(readFromFile(fileName));
            int divider = fileName.indexOf(NoteAdapter.NOTE_NAME);
            String sub = fileName.substring(divider + 1,fileName.length());
            etTitle.setText(sub);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        prefs = getSharedPreferences(SettingsActivity.SHARED_PREFS, MODE_PRIVATE);
        ed = prefs.edit();
        String color;
        color = prefs.getString(SettingsActivity.SAVED_TEXT_COLOR, getString(R.string.text_black));

        int size;
        size = prefs.getInt(SettingsActivity.SAVED_TEXT_SIZE, 18);
        Log.d("DEBUGER","" + size + "   " + prefs.getInt(SettingsActivity.SAVED_TEXT_SIZE,18));

        etNote.setTextColor(Color.parseColor(color));
        etTitle.setTextColor(Color.parseColor(color));

        etNote.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        etTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(NoteActivity.this,SettingsActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

            if(fileName == null||!etNote.isEnabled()){
                editOrSaveNotes();
            }else{
                File file = new File(getString(R.string.directory_file_path) + fileName);
                if (file.exists()){
                    file.delete();
                }
                editOrSaveNotes();
            }

    }

    private void editOrSaveNotes(){
        if(!etNote.isEnabled()){
            etNote.setEnabled(true);
            etTitle.setEnabled(true);
            mFabEditAndSave.setImageResource(R.drawable.ic_save);
        }else{
            etNote.setEnabled(false);
            etTitle.setEnabled(false);
            mFabEditAndSave.setImageResource(R.drawable.ic_edit);

            writeANote(etTitle.getText().toString(),etNote.getText().toString());
        }
    }

    private String changeTextMonthToNumbers(String s){

        if(s.contains("Jan")){
            s = s.replace("Jan","01");
        }else if(s.contains("Feb")){
            s = s.replace("Feb","02");
        }else if(s.contains("Mar")){
            s = s.replace("Mar","03");
        }else if(s.contains("Apr")){
            s = s.replace("Apr","04");
        }else if(s.contains("May")){
            s = s.replace("May","05");
        }else if(s.contains("Jun")){
            s = s.replace("Jun","06");
        }else if(s.contains("Jul")){
            s = s.replace("Jul","07");
        }else if(s.contains("Aug")){
            s = s.replace("Aug","08");
        }else if(s.contains("Sep")){
            s = s.replace("Sep","09");
        }else if(s.contains("Oct")){
            s = s.replace("Oct","10");
        }else if(s.contains("Nov")){
            s = s.replace("Nov","11");
        }else if(s.contains("Dec")){
            s = s.replace("Dec","12");
        }

        String year = s.substring(s.length()-4,s.length());
        s = s.replace(year,"");
        s = year + s;
        return s;
    }



    public void writeANote(String path, String dataToSave) {

        if(path.length()>0||dataToSave.length()>0){

            String date = new Date().toString();
            int dividerFrom = date.indexOf("GMT");
            int dividerTo = date.indexOf(" ", dividerFrom);
            String replace = date.substring(dividerFrom, dividerTo);
            date = date.replace(replace,"");
            date = changeTextMonthToNumbers(date);
            date = date.replaceAll("[a-zA-Z]","").trim();
            path = date + "_" + path;

            try {
                File file = new File(getString(R.string.directory),path);
                FileOutputStream fos = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(fos);

                osw.write(dataToSave + "\n");
                osw.flush();
                osw.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String readFromFile(String path) {

        String ret = "";
        File file = new File(getString(R.string.directory_file_path) + path);

//Get the text file


//Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            ret = new String(text);
            br.close();
        } catch (IOException e) {
//You'll need to add proper error handling here
        }

        return ret;
    }

}
