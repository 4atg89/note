package com.example.atg.note;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.Arrays;
import java.util.List;

public class SettingsActivity extends Activity implements AdapterView.OnItemSelectedListener {

    public static final String SAVED_TEXT_COLOR = "keyToSettingColor";
    public static final String SAVED_TEXT_SIZE = "keyToSettingSize";
    public static final String SHARED_PREFS = "ColorText";

    private Spinner spinTextSize, spinTextColor;
    private SharedPreferences prefs;
    private SharedPreferences.Editor ed;
    private boolean firstPositionSize;
    private boolean firstPositionColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        prefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        ed = prefs.edit();

        spinTextColor = (Spinner) findViewById(R.id.spinTextColor);
        spinTextSize = (Spinner) findViewById(R.id.spinTextSize);

        Integer[] textSize = {0,R.dimen.text_size15, R.dimen.text_size16, R.dimen.text_size17,
                R.dimen.text_size18, R.dimen.text_size19, R.dimen.text_size20, R.dimen.text_size21,
                R.dimen.text_size22, R.dimen.text_size23, R.dimen.text_size24, R.dimen.text_size25,
                R.dimen.text_size26, R.dimen.text_size27, R.dimen.text_size28, R.dimen.text_size29,
                R.dimen.text_size30};

        List<Integer> textSizeList = Arrays.asList(textSize);

        TextSizeAdapter textSizeAdapter = new TextSizeAdapter(this, textSizeList);
        spinTextSize.setAdapter(textSizeAdapter);
        spinTextSize.setOnItemSelectedListener(this);

        String textColor[] = {"",getString(R.string.text_black), getString(R.string.text_blue),
                getString(R.string.text_dark_blue), getString(R.string.text_orange), getString(R.string.text_red),
                getString(R.string.text_yellow), getString(R.string.text_green), getString(R.string.text_purple)};

        List<String> colorList = Arrays.asList(textColor);

        TextColorAdapter textColorAdapter = new TextColorAdapter(this, colorList);
        spinTextColor.setAdapter(textColorAdapter);
        spinTextColor.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int posColor = spinTextColor.getSelectedItemPosition();
        int posSize = spinTextSize.getSelectedItemPosition();

        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.spinTextColor) {
            switch (posColor) {
                case 0:

                    break;
                case 1:
                        ed.putString(SAVED_TEXT_COLOR, getString(R.string.text_black));
                        ed.commit();
                    break;
                case 2:
                    ed.putString(SAVED_TEXT_COLOR, getString(R.string.text_blue));
                    ed.commit();
                    break;
                case 3:
                    ed.putString(SAVED_TEXT_COLOR, getString(R.string.text_dark_blue));
                    ed.commit();
                    break;
                case 4:
                    ed.putString(SAVED_TEXT_COLOR, getString(R.string.text_orange));
                    ed.commit();
                    break;
                case 5:
                    ed.putString(SAVED_TEXT_COLOR, getString(R.string.text_red));
                    ed.commit();
                    break;
                case 6:
                    ed.putString(SAVED_TEXT_COLOR, getString(R.string.text_yellow));
                    ed.commit();
                    break;
                case 7:
                    ed.putString(SAVED_TEXT_COLOR, getString(R.string.text_green));
                    ed.commit();
                    break;
                case 8:
                    ed.putString(SAVED_TEXT_COLOR, getString(R.string.text_purple));
                    ed.commit();
                    break;
            }
        } else if (spinner.getId() == R.id.spinTextSize) {
            switch (posSize) {
                case 0:

                    break;
                case 1:
                    ed.putInt(SAVED_TEXT_SIZE, 15);
                    ed.commit();
                    break;
                case 2:
                    ed.putInt(SAVED_TEXT_SIZE, 16);
                    ed.commit();
                    break;
                case 3:
                    ed.putInt(SAVED_TEXT_SIZE, 17);
                    ed.commit();
                    break;
                case 4:
                    ed.putInt(SAVED_TEXT_SIZE, 18);
                    ed.commit();
                    break;
                case 5:
                    ed.putInt(SAVED_TEXT_SIZE, 19);
                    ed.commit();
                    break;
                case 6:
                    ed.putInt(SAVED_TEXT_SIZE, 20);
                    ed.commit();
                    break;
                case 7:
                    ed.putInt(SAVED_TEXT_SIZE, 21);
                    ed.commit();
                    break;
                case 8:
                    ed.putInt(SAVED_TEXT_SIZE, 22);
                    ed.commit();
                    break;
                case 9:
                    ed.putInt(SAVED_TEXT_SIZE, 23);
                    ed.commit();
                    break;
                case 10:
                    ed.putInt(SAVED_TEXT_SIZE, 24);
                    ed.commit();
                    break;
                case 11:
                    ed.putInt(SAVED_TEXT_SIZE, 25);
                    ed.commit();
                    break;
                case 12:
                    ed.putInt(SAVED_TEXT_SIZE, 26);
                    ed.commit();
                    break;
                case 13:
                    ed.putInt(SAVED_TEXT_SIZE, 27);
                    ed.commit();
                    break;
                case 14:
                    ed.putInt(SAVED_TEXT_SIZE, 28);
                    ed.commit();
                    break;
                case 15:
                    ed.putInt(SAVED_TEXT_SIZE, 29);
                    ed.commit();
                    break;
                case 16:
                    Log.d("DEBUGER", "" + 30 + "   ");
                    ed.putInt(SAVED_TEXT_SIZE, 30);
                    ed.commit();
                    break;
            }


        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
