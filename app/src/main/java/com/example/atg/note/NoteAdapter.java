package com.example.atg.note;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter {

    public final static String NOTE_NAME = "_";

    private List<File> mList;
    private Context mContext;
    private NoteOperations noteOperations;

    public NoteAdapter(Context context, NoteOperations noteDelete, List<File> list) {
        mList = list;
        mContext = context;
        noteOperations = noteDelete;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.title_in_recycler, parent, false);
        return new NoteHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NoteHolder noteHolder = (NoteHolder) holder;
        int divider = mList.get(position).getName().indexOf(NOTE_NAME);
        String sub = mList.get(position).getName().substring(0, divider);
        noteHolder.tvDate.setText(sub);
        sub = mList.get(position).getName().substring(divider + 1, mList.get(position).getName().length());
        noteHolder.tvTitle.setText(sub);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    private class NoteHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Button bDelete;
        private TextView tvTitle;
        private TextView tvDate;
        private RelativeLayout rlOpenNote;

        public NoteHolder(View itemView) {
            super(itemView);
            bDelete = (Button) itemView.findViewById(R.id.bDelete);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            rlOpenNote = (RelativeLayout)itemView.findViewById(R.id.rlOpenNote);
            bDelete.setOnClickListener(this);
            rlOpenNote.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.bDelete:
                    noteOperations.deleteANote(mList.get(getAdapterPosition()).getName());
                    mList.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    break;
                case R.id.rlOpenNote:
                    noteOperations.openTheNote(tvDate.getText().toString() + NOTE_NAME + tvTitle.getText().toString());
                    break;
            }

        }
    }
}
