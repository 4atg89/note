package com.example.atg.note;

public class Note {

    private String mTitle;
    private String mNote;

    public Note(String mTitle, String mNote) {
        this.mTitle = mTitle;
        this.mNote = mNote;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmNote() {
        return mNote;
    }
}
