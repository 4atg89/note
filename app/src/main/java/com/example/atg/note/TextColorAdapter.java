package com.example.atg.note;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class TextColorAdapter extends BaseAdapter{

    private List<String> mTextColors;
    private Context mContext;

    public TextColorAdapter(Context context,List<String> textColors) {
        mContext = context;
        mTextColors = textColors;

    }


    @Override
    public int getCount() {
        return mTextColors.size();
    }

    @Override
    public String getItem(int position) {
        return mTextColors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.setting_text_color,null);
        TextView tvColor = (TextView) view.findViewById(R.id.tvColorSetting);
        if(position == 0){
            tvColor.setText("Выберите цвет текста.");
        }else {
            tvColor.setTextColor(Color.parseColor(mTextColors.get(position)));
            tvColor.setText("Выбрать");
        }
        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.setting_text_color,null);
            holder = new ViewHolder();
            holder.tvColor = (TextView)convertView.findViewById(R.id.tvColorSetting);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        if(position == 0){
            holder.tvColor.setText("Выберите цвет текста.");
        }else {
            holder.tvColor.setTextColor(Color.parseColor(mTextColors.get(position)));
            holder.tvColor.setText("Выбрать");
        }

        return convertView;
    }

    private class ViewHolder{

        TextView tvColor;
    }
}
