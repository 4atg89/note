package com.example.atg.note;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NoteOperations {

    public final static String NOTE_PATH = "main to note";

    private List<File> mList;
    private RecyclerView mRecyclerNote;
    private FloatingActionButton mFabAddNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerNote = (RecyclerView) findViewById(R.id.recyclerNote);
        mFabAddNote = (FloatingActionButton) findViewById(R.id.fabAddNote);
        mFabAddNote.setOnClickListener(this);

        mList = new ArrayList<>();

        createFolder();

    }

    @Override
    protected void onResume() {
        super.onResume();
        readAllNotesInFolder();
        NoteAdapter adapter = new NoteAdapter(this, this, mList);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerNote.setLayoutManager(manager);
        mRecyclerNote.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void readAllNotesInFolder() {
        mList = new ArrayList<>();

        File f = new File(getString(R.string.directory));
        File file[] = f.listFiles();
        if(file.length > 0){
            int length = file.length;
            for (int i = 0; i < length; i++) {
                mList.add(new File(file[i].getName()));
            }
            Collections.sort(mList);
            Collections.reverse(mList);
        }
    }

    public void createFolder() {
        File file = new File(getString(R.string.directory));
        file.mkdirs();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, NoteActivity.class);
        startActivity(intent);
    }

    @Override
    public void deleteANote(String fileName) {
        File file = new File(getString(R.string.directory_file_path) + fileName);
        if (file.exists()){
            file.delete();
        }
    }

    @Override
    public void openTheNote(String fileName) {
        Intent i = new Intent(this,NoteActivity.class);
        i.putExtra(NOTE_PATH,fileName);
        startActivity(i);
    }
}
